<?php

namespace App\Exports;

use App\Gmv;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class GmvExport implements FromArray, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize
{
    protected $from;
    protected $to;
    protected $gmv;
    protected $brands;

    public function __construct($range,$gmv, $brands)
    {
        $this->from = $range['from'];
        $this->to = $range['to'];
        $this->gmv = $gmv;
        $this->brands = $brands;

    }

    public function array(): array
    {
        return $this->gmv;
    }

    /**
     * @var Gmv $gmv
     * @return array
     */
    public function map($gmv): array
    {
        return [
            $gmv
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            'O-Brand',
            'T-Brand',
            'R-Brand',
            'I-Brand',
            'U-Brand',
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [

        ];
    }
}
