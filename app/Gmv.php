<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gmv extends Model
{
    protected static $logFillable = true;
    protected  $table = 'gmv';

    public function getDescriptionForEvent(string $eventName): string
    {
        $model = class_basename($this);
        return "{$model} has been {$eventName}";
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'brand_id', 'date', 'turnover'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

        /**
     * get brands
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand() {
        return $this->belongsTo('App\Brands', 'brand_id');
    }
}
