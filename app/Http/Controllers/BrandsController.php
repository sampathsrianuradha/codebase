<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\Exports\BrandsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException; 

class BrandsController extends Controller
{
    /**
     * Brands index page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $Brands = Brands::all();
            return Excel::download(new BrandsExport, 'brands.csv');

        } catch (NotFoundHttpException $e) {

            return response(['data'=>[], 'msg' => $e->getMessage(), 'status'=>'error'], 404);

        } catch (ModelNotFoundException $e) {

            return response(['data'=>[], 'msg' => $e->getMessage(), 'status'=>'error'], 500);

        }
    }
}
