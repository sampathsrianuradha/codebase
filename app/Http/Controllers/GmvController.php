<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Gmv;
use App\Exports\GmvExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException; 

class GmvController extends Controller
{
    /**
     * Gmv index page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $from = date('2018-05-01');
            $to   = date('2018-05-07');
            $range = array(
                'from' => $from,
                'to'   => $to
            );

            $gmv = Gmv::whereBetween('date', [$from, $to])
                ->with(['brand' => function ($q) {
                    $q->orderBy('name','asc');
                }])->get();
            $gmv = $gmv->toArray();

            $period = $this->dateRange( $from, $to );
            $row = [];
            foreach ($period as $index => $dt) {
                $row[$index]['date'] = $dt;
                foreach ($gmv as $key => $value) {
                    if(date("Y-m-d", strtotime($value['date']) )== $dt){
                        $row[$index][$value['brand']['name']] = $value['turnover'];
                        unset($gmv[$key]);
                    }
                }
            }
            $brands = array_unique(array_column($gmv, 'brand_id'));
            $brands = array_intersect_key($gmv, $brands);

            $export = new GmvExport(
                $range,
                $row,
                $brands
            );
            return Excel::download($export, 'gmv.xlsx');

        } catch (NotFoundHttpException $e) {

            return response(['data'=>[], 'msg' => $e->getMessage(), 'status'=>'error'], 404);

        } catch (ModelNotFoundException $e) {

            return response(['data'=>[], 'msg' => $e->getMessage(), 'status'=>'error'], 500);

        }
    }

    /**
     * Date range
     *
     * @param $first
     * @param $last
     * @param string $step
     * @param string $format
     * @return array
     */
    public function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
        $dates = [];
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {

            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }
}
